#pragma once

//Utils
#include "Hooking.hpp"
#include "Utils.hpp"

//Functions
#include "..\\Modules\Functions.hpp"

//Patches
#include "..\\Modules\XPatchAds.hpp"
#include "..\\Modules\XPatchBitrate.hpp"
#include "..\\Modules\XPatchDownloads.hpp"
#include "..\\Modules\XPatchAutoUpdates.hpp"
#include "..\\Modules\XPatchInit.hpp"

