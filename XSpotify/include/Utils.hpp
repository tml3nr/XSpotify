#pragma once
#include "BaseInclude.hpp"

namespace Utils
{
	class Utils
	{
	public:
		static void PressMediaKey(int key);
		static void removeForbiddenChar(std::string* s);
		static void handleErrors(void);
		static void deleteFile(const std::wstring& filename);

		static int GetFileSize(std::string file);

		static bool FileExists(std::string file);
		static bool PathExists(std::string path);

		static std::string hex2string(std::string str);
		static std::string b64_encode(BYTE const* buf, unsigned int bufLen);
		static std::wstring ConvertUtf8ToUtf16(const std::string& str);
	};

}