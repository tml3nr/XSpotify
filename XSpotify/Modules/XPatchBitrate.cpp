#include "..\\include\BaseInclude.hpp"

namespace Modules
{
	void __declspec(naked) Bitrate::SelectOGGFormat_stub(int a1, int a2, int a3)
	{
		__asm
		{
			cmp     eax, 96000
			jz      OGG_VORBIS_320
			cmp     eax, 160000
			jz      OGG_VORBIS_320
			cmp     eax, 320000
			jz      OGG_VORBIS_320

			OGG_VORBIS_320 :
			push    0C52342h
				retn
		}
	}

	signed int Bitrate::SetBitrate_hk(int a1)
	{
		return 320000;
	}

	signed int Bitrate::SetBitrate2_hk(int a1)
	{
		return 320000;
	}

	void __declspec(naked) Bitrate::ExecBitrate_stub(DWORD* a1, void* a2)
	{
		__asm
		{
			cmp     ecx, 9
			ja      defaultCase
			jmp		highBitrateCase

			defaultCase :
			push	0E33B05h
			retn

			highBitrateCase :
			push	0E33AD1h
			retn

		}
	}

	void __declspec(naked) Bitrate::CheckBitrate_stub(signed int* a1, int a2, int a3, int a4, int a5)
	{
		__asm
		{
			cmp     esi, 160000
			jle		jmpElse
			test    bl, bl
			mov     eax, 320000
			push	0E055C3h
			retn

			jmpElse :
			push	0E055C6h
				retn
		}
	}

	int Bitrate::SyncBitrateEnumeration_hk(signed int a1)
	{
		return 4;
	}

	void __declspec(naked) Bitrate::BitrateSetting_stub(int a1, int a2, int a3, int* a4)
	{
		__asm
		{
			mov     edi, 4
			cmp     edi, 4
			ja		defaultCase
			jmp     veryHigh

			defaultCase :
			push	071822Bh
				retn

				veryHigh :
			push	0718224h
				retn
		}
	}

	Bitrate::Bitrate()
	{

		//320kbits but wrong codec
		//Hook::InstallJmp((void*)0xE057B0, SetBitrate_hk);
		Utils::Hook::InstallJmp((void*)0xE33AA1, Bitrate::ExecBitrate_stub); // without ExecBitrate we get the right codec, but we cant play the songs 
		Utils::Hook::InstallJmp((void*)0xE055B4, Bitrate::CheckBitrate_stub);
		Utils::Hook::InstallJmp((void*)0x7181FC, Bitrate::BitrateSetting_stub);
	}
		
}

